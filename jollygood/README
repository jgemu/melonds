melonDS
-------
melonDS is an emulator for the Nintendo DS and DSi. The Jolly Good API port is
designed for use with content for the Nintendo DS.

Source for the emulator core is unmodified from upstream sources.

This repository lives at https://gitlab.com/jgemu/melonds

Compiling
---------
Make sure you have The Jolly Good API's header files installed. If you did
not install them, you will be required to include their path in CXXFLAGS.

GNU Make's default behaviour for compiling C++ sources is to use g++. If your
platform of choice uses an unpatched GNU Make, you will need to override the
CXX implicit variable if you wish to use a different compiler.

Options:
  DISABLE_MODULE - Set to a non-zero value to disable building the module.
  ENABLE_STATIC_JG - Set to a non-zero value to build a static JG archive.
  USE_VENDORED_SAMPLERATE - Set non-zero to use vendored libsamplerate

Linux:
  make

macOS:
  make

BSD:
  gmake

Windows (MSYS2):
  make

Cross Compile:
(For example compiling on Linux for MinGW)
  AR=x86_64-w64-mingw32-ar \
  CC=x86_64-w64-mingw32-cc \
  CXX=x86_64-w64-mingw32-c++ \
  PKG_CONFIG=x86_64-w64-mingw32-pkg-config \
  STRIP=x86_64-w64-mingw32-strip \
  make

The build will be output to "melonds/". This directory may be used as is
locally by copying it to your local "cores" directory, or may be installed
system-wide using the "install" target specified in the Makefile.

Copyright
---------
melonDS Jolly Good API Port (GPL-3.0-or-later)
  Copyright (c) 2022 Rupert Carmichael

libsamplerate (BSD-2-Clause)
  Copyright (c) 2002-2021, Erik de Castro Lopo
  See deps/libsamplerate/COPYING (https://github.com/libsndfile/libsamplerate)
