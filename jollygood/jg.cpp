/*
 * melonDS - Jolly Good API Port
 *
 * Copyright (C) 2022-2023 Rupert Carmichael
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, specifically version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <chrono>
#include <condition_variable>
#include <fstream>
#include <mutex>
#include <thread>
#include <vector>
#include <cstdarg>

#include <jg/jg.h>
#include <jg/jg_nds.h>

#include <samplerate.h>

#include "Platform.h"
#include "FreeBIOS.h"
#include "NDS.h"
#include "GPU.h"
#include "SPU.h"

#include "version.h"

#define SAMPLERATE 48000
#define SAMPLERATE_IN 32823.6328125
#define FRAMERATE 60
#define CHANNELS 2
#define NUMINPUTS 2

static jg_cb_audio_t jg_cb_audio;
static jg_cb_frametime_t jg_cb_frametime;
static jg_cb_log_t jg_cb_log;
static jg_cb_rumble_t jg_cb_rumble;

static jg_coreinfo_t coreinfo = {
    "melonds", "melonDS",
    JG_VERSION,
    "nds",
    NUMINPUTS,
    JG_HINT_INPUT_AUDIO | JG_HINT_INPUT_VIDEO
};

static jg_videoinfo_t vidinfo = {
    JG_PIXFMT_XRGB8888, // pixfmt
    256,                // wmax
    384,                // hmax
    256,                // w
    384,                // h
    0,                  // x
    0,                  // y
    256,                // p
    256.0/384.0,        // aspect
    NULL                // buf
};

static jg_audioinfo_t audinfo = {
    JG_SAMPFMT_FLT32,
    SAMPLERATE,
    CHANNELS,
    (SAMPLERATE / FRAMERATE) * CHANNELS,
    NULL
};

static jg_pathinfo_t pathinfo;
static jg_fileinfo_t gameinfo;
static jg_inputinfo_t inputinfo[NUMINPUTS];
static jg_inputstate_t *input_device[NUMINPUTS];

// Resampling
static SRC_STATE *srcstate = nullptr;
static SRC_DATA srcdata;
static int16_t inbuf[((SAMPLERATE / FRAMERATE) * CHANNELS) << 1];
static float inbuf_f[((SAMPLERATE / FRAMERATE) * CHANNELS) << 1];

/* The JGSemaphore class implements just enough features for this emulator core
   to function. Since C++ did not get semaphores until C++20, and melonDS is
   a C++17 codebase, this implementation is required.
*/
class JGSemaphore {
private:
    std::condition_variable cv;
    std::mutex mtx;
    int count;

public:
    JGSemaphore(int c = 0) : count(c) { }

    void acquire(int n = 1) {
        std::unique_lock<std::mutex> lck(mtx);
        while(count < n)
            cv.wait(lck);
        count -= n;
    }

    int available() const { return count; }

    void release(int n = 1) {
        std::lock_guard<std::mutex> lck(mtx);
        count += n;
        cv.notify_all();
    }
};

namespace Platform {

void Init(int argc, char** argv) {
}

void DeInit() {
}

void StopEmu() {
}

int InstanceID() {
    return 0;
}

std::string InstanceFileSuffix() {
    return "";
}

bool FileFlush(FileHandle* file) {
    return fflush(reinterpret_cast<FILE *>(file)) == 0;
}

u64 FileLength(FileHandle* file) {
    FILE* stdfile = reinterpret_cast<FILE *>(file);
    long pos = ftell(stdfile);
    fseek(stdfile, 0, SEEK_END);
    long len = ftell(stdfile);
    fseek(stdfile, pos, SEEK_SET);
    return len;
}

u64 FileRead(void* data, u64 size, u64 count, FileHandle* file) {
    return fread(data, size, count, reinterpret_cast<FILE *>(file));
}

bool FileReadLine(char* str, int count, FileHandle* file) {
    return fgets(str, count, reinterpret_cast<FILE *>(file)) != nullptr;
}

void FileRewind(FileHandle* file) {
    rewind(reinterpret_cast<FILE *>(file));
}

bool FileSeek(FileHandle* file, s64 offset, FileSeekOrigin origin) {
    int stdorigin;
    switch (origin)
    {
        case FileSeekOrigin::Start: stdorigin = SEEK_SET; break;
        case FileSeekOrigin::Current: stdorigin = SEEK_CUR; break;
        case FileSeekOrigin::End: stdorigin = SEEK_END; break;
    }

    return fseek(reinterpret_cast<FILE *>(file), offset, stdorigin) == 0;
}

u64 FileWrite(const void* data, u64 size, u64 count, FileHandle* file) {
    return fwrite(data, size, count, reinterpret_cast<FILE *>(file));
}

u64 FileWriteFormatted(FileHandle* file, const char* fmt, ...) {
    if (fmt == nullptr)
        return 0;

    va_list args;
    va_start(args, fmt);
    u64 ret = vfprintf(reinterpret_cast<FILE *>(file), fmt, args);
    va_end(args);
    return ret;
}

int GetConfigInt(ConfigEntry entry) {
    switch (entry) {
        case AudioBitDepth: {
            return 2; // 16-bit
        }
        default: return 0;
    }

    return 0;
}

bool GetConfigBool(ConfigEntry entry) {
    switch (entry) {
        default: case ExternalBIOSEnable:
            return 0;
    }

    return false;
}

std::string GetConfigString(ConfigEntry entry) {
    return "";
}

bool GetConfigArray(ConfigEntry entry, void* data) {
    return false;
}

constexpr char AccessMode(FileMode mode, bool file_exists)
{
    if (!(mode & FileMode::Write))
        return 'r';

    if (mode & (FileMode::NoCreate))
        return 'r';

    if ((mode & FileMode::Preserve) && file_exists)
        return 'r';

    return 'w';
}

bool IsEndOfFile(FileHandle* file)
{
    return feof(reinterpret_cast<FILE *>(file)) != 0;
}

constexpr bool IsExtended(FileMode mode)
{
    // fopen's "+" flag always opens the file for read/write
    return (mode & FileMode::ReadWrite) == FileMode::ReadWrite;
}

static std::string GetModeString(FileMode mode, bool file_exists)
{
    std::string modeString;

    modeString += AccessMode(mode, file_exists);

    if (IsExtended(mode))
        modeString += '+';

    if (!(mode & FileMode::Text))
        modeString += 'b';

    return modeString;
}

FileHandle* OpenFile(const std::string& path, FileMode mode) {
    std::string modeString = GetModeString(mode, true);
    FILE* file = fopen(path.c_str(), modeString.c_str());
    if (file)
    {
        Log(LogLevel::Debug,
            "Opened \"%s\" with FileMode 0x%x (effective mode \"%s\")\n",
            path.c_str(), mode, modeString.c_str());
        return reinterpret_cast<FileHandle *>(file);
    }
    else
    {
        Log(LogLevel::Warn,
            "Failed to open \"%s\" with FileMode 0x%x (effective mode \"%s\")"
            "\n", path.c_str(), mode, modeString.c_str());
        return nullptr;
    }
}

FileHandle* OpenLocalFile(const std::string& path, FileMode mode) {
    return OpenFile(path, mode);
}

bool CloseFile(FileHandle* file)
{
    return fclose(reinterpret_cast<FILE *>(file)) == 0;
}

bool LocalFileExists(const std::string& name)
{
    FileHandle* f = OpenLocalFile(name, FileMode::Read);
    if (!f) return false;
    CloseFile(f);
    return true;
}

Thread* Thread_Create(std::function<void()> func) {
    return (Thread*)new std::thread(func);
}

void Thread_Free(Thread* thread) {
    std::thread *th = (std::thread*)thread;
    /* melonDS' reference implementation uses Qt threads, so the below
       Thread_Wait typically calls Qt's QThread::wait before deleting the
       thread. QThread::wait is similar to pthread_join or std::thread::join.
       This call to std::thread::join is here for safety.
    */
    if (th->joinable())
        th->join();
    delete th;
}

void Thread_Wait(Thread* thread) {
    ((std::thread*)thread)->join();
}

Semaphore* Semaphore_Create() {
    return (Semaphore*)new JGSemaphore;
}

void Semaphore_Free(Semaphore* sema) {
    delete (JGSemaphore*)sema;
}

void Semaphore_Reset(Semaphore* sema) {
    JGSemaphore *s = (JGSemaphore*)sema;
    s->acquire(s->available());
}

void Semaphore_Wait(Semaphore* sema) {
    ((JGSemaphore*)sema)->acquire();
}

void Semaphore_Post(Semaphore* sema, int count) {
    ((JGSemaphore*)sema)->release(count);
}

Mutex* Mutex_Create() {
    return (Mutex*)new std::mutex;
}

void Mutex_Free(Mutex* mutex) {
    delete (std::mutex*)mutex;
}

void Mutex_Lock(Mutex* mutex) {
    ((std::mutex*)mutex)->lock();
}

void Mutex_Unlock(Mutex* mutex) {
    ((std::mutex*)mutex)->unlock();
}

bool Mutex_TryLock(Mutex* mutex) {
    return ((std::mutex*)mutex)->try_lock();
}

void SignalStop(StopReason reason) {
    jg_deinit();

    switch (reason) {
        case StopReason::GBAModeNotSupported:
            Log(LogLevel::Error, "!! GBA MODE NOT SUPPORTED\n");
            break;
        case StopReason::BadExceptionRegion:
            Log(LogLevel::Error, "Internal error.");
            break;
        case StopReason::PowerOff:
        case StopReason::External:
            Log(LogLevel::Error, "Shutdown");
        default:
            break;
    }
}

void Sleep(u64 usecs) {
    std::this_thread::sleep_for(std::chrono::microseconds(usecs));
}

void WriteNDSSave(const u8* savedata, u32 savelen, u32, u32) {
    std::string path =
        std::string(pathinfo.save) + "/" + std::string(gameinfo.name) + ".sav";
    std::ofstream stream(path, std::ios::out | std::ios::binary);

    if (stream.is_open()) {
        stream.write((const char*)savedata, savelen);
        stream.close();
        jg_cb_log(JG_LOG_DBG, "File saved %s\n", path.c_str());
    }
    else {
        jg_cb_log(JG_LOG_WRN, "Failed to save file: %s\n", path.c_str());
    }
}

void WriteGBASave(const u8*, u32, u32, u32) {
}

void WriteFirmware(const Firmware& firmware, u32, u32) {
}

void WriteDateTime(int, int, int, int, int, int) {
}

bool MP_Init() {
    return false;
}

void MP_DeInit() {
}

void MP_Begin() {
}

void MP_End() {
}

int MP_SendPacket(u8* data, int len, u64 timestamp) {
    return 0;
}

int MP_RecvPacket(u8* data, u64* timestamp) {
    return 0;
}

int MP_SendCmd(u8* data, int len, u64 timestamp) {
    return 0;
}

int MP_SendReply(u8* data, int len, u64 timestamp, u16 aid) {
    return 0;
}

int MP_SendAck(u8* data, int len, u64 timestamp) {
    return 0;
}

int MP_RecvHostPacket(u8* data, u64* timestamp) {
    return 0;
}

u16 MP_RecvReplies(u8* data, u64 timestamp, u16 aidmask) {
    return 0;
}

bool LAN_Init() {
    return false;
}

void LAN_DeInit() {
}

int LAN_SendPacket(u8* data, int len) {
    return 0;
}

int LAN_RecvPacket(u8* data) {
    return 0;
}

void Camera_Start(int num) {
}

void Camera_Stop(int num) {
}

void Camera_CaptureFrame(int num, u32* frame, int width, int height, bool yuv) {
}

void Log(LogLevel level, const char* fmt, ...) {
    jg_cb_log(level, fmt);
}

} // namespace Platform

static const int NDSMap[] = {
    6, 7, 5, 4, 2, 3, 0, 1, 10, 11, 9, 8
};

static void mds_input_refresh(void) {
    // Buttons
    u32 buttons = 0xfff;

    for (int i = 0; i < NDEFS_NDS; ++i)
        if (input_device[0]->button[i]) buttons &= ~(1 << NDSMap[i]);

    NDS::SetKeyMask(buttons);

    // Touchscreen
    if (input_device[1]->button[0] && input_device[1]->coord[1] >= 192)
        NDS::TouchScreen(input_device[1]->coord[0],
            input_device[1]->coord[1] - 192);
    else
        NDS::ReleaseScreen();
}

static void mds_audio_push(void) {
    u32 insamps = NDS::SPU->GetOutputSize();
    NDS::SPU->ReadOutput(inbuf, insamps);

    src_short_to_float_array(inbuf, inbuf_f, insamps << 1);
    srcdata.data_in = inbuf_f;
    srcdata.data_out = (float*)audinfo.buf;
    srcdata.input_frames = insamps;
    srcdata.output_frames = audinfo.spf;
    src_process(srcstate, &srcdata);

    jg_cb_audio(srcdata.output_frames_gen << 1);
}

void jg_set_cb_audio(jg_cb_audio_t func) {
    jg_cb_audio = func;
}

void jg_set_cb_frametime(jg_cb_frametime_t func) {
    jg_cb_frametime = func;
}

void jg_set_cb_log(jg_cb_log_t func) {
    jg_cb_log = func;
}

void jg_set_cb_rumble(jg_cb_rumble_t func) {
    jg_cb_rumble = func;
}

int jg_init(void) {
    Melon::RenderSettings vsettings;
    vsettings.Soft_Threaded = true;

    memcpy(NDS::ARM9BIOS, bios_arm9_bin, sizeof(bios_arm9_bin));
    memcpy(NDS::ARM7BIOS, bios_arm7_bin, sizeof(bios_arm7_bin));

    NDS::Init();

    int videoRenderer = 0; // Software rendering, GL not currently enabled
    NDS::GPU->InitRenderer(videoRenderer);
    NDS::GPU->SetRenderSettings(videoRenderer, vsettings);

    NDS::SPU->SetInterpolation(0); // Just leave the audio samples alone

    NDS::SetConsoleType(0); // Hardcoded for DS

    return 1;
}

void jg_deinit(void) {
    NDS::GPU->DeInitRenderer();
    NDS::DeInit();

    if (srcstate)
        srcstate = src_delete(srcstate);
    srcstate = nullptr;
}

void jg_reset(int) {
    NDS::Reset();
    NDS::SetupDirectBoot(gameinfo.fname);
    NDS::Start();
}

void jg_exec_frame(void) {
    // Refresh input, run a frame, push audio to resampler
    mds_input_refresh();
    NDS::RunFrame();
    mds_audio_push();

    // Draw each screen's internal buffer to the shared output buffer
    size_t scrsz = (256 * 192 * sizeof(u32)); // Size of a single screen
    u8 *vbuf0 = (u8*)vidinfo.buf;
    u8 *vbuf1 = vbuf0 + scrsz;
    memcpy(vbuf0, NDS::GPU->Framebuffer[NDS::GPU->GPU::FrontBuffer][0], scrsz);
    memcpy(vbuf1, NDS::GPU->Framebuffer[NDS::GPU->GPU::FrontBuffer][1], scrsz);
}

int jg_game_load(void) {
    // Load the BIOS and Cartridge
    NDS::LoadBIOS();
    NDS::LoadCart((u8*)gameinfo.data, gameinfo.size, nullptr, 0);

    // Load the save data
    std::string savepath =
        std::string(pathinfo.save) + "/" + std::string(gameinfo.name) + ".sav";
    std::ifstream stream(savepath, std::ios::in | std::ios::binary);

    if (stream.is_open()) {
        std::vector<u8> savedata((std::istreambuf_iterator<char>(stream)),
            std::istreambuf_iterator<char>());
        NDS::LoadSave(savedata.data(), savedata.size());
    }
    else {
        jg_cb_log(JG_LOG_DBG, "Failed to load file: %s\n", savepath.c_str());
    }

    // Set up input devices
    inputinfo[0] = jg_nds_inputinfo(JG_NDS_SYSTEM, 0);
    inputinfo[1] = jg_nds_inputinfo(JG_NDS_TOUCH, 1);

    return 1;
}

int jg_game_unload(void) {
    return 1;
}

int jg_state_load(const char *filename) {
    std::ifstream stream(filename, std::ios::in | std::ios::binary);
    std::vector<uint8_t> statefile((std::istreambuf_iterator<char>(stream)),
        std::istreambuf_iterator<char>());
    stream.close();

    Savestate *st = new Savestate(statefile.data(), statefile.size(), false);
    bool ret = NDS::DoSavestate(st);
    delete st;
    return ret;

}

void jg_state_load_raw(const void *data) {
}

int jg_state_save(const char *filename) {
    Savestate *st = new Savestate;
    bool ret = false;

    if (NDS::DoSavestate(st)) {
        std::ofstream stream(filename, std::ios::out | std::ios::binary);
        if (stream.is_open()) {
            stream.write((const char*)st->Buffer(), st->Length());
            stream.close();
            ret = true;
        }
    }

    delete st;
    return ret;
}

const void* jg_state_save_raw(void) {
    return NULL;
}

size_t jg_state_size(void) {
    return 0;
}

void jg_media_select(void) {
}

void jg_media_insert(void) {
}

void jg_cheat_clear(void) {
}

void jg_cheat_set(const char *code) {
}

void jg_rehash(void) {
}

void jg_data_push(uint32_t, int, const void*, size_t) {
}

jg_coreinfo_t* jg_get_coreinfo(const char *sys) {
    return &coreinfo;
}

jg_videoinfo_t* jg_get_videoinfo(void) {
    return &vidinfo;
}

jg_audioinfo_t* jg_get_audioinfo(void) {
    return &audinfo;
}

jg_inputinfo_t* jg_get_inputinfo(int port) {
    return &inputinfo[port];
}

jg_setting_t* jg_get_settings(size_t *numsettings) {
    *numsettings = 0;
    return NULL;
}

void jg_setup_video(void) {
}

void jg_setup_audio(void) {
    if (srcstate == nullptr) {
        int err;
        srcstate = src_new(SRC_SINC_FASTEST, CHANNELS, &err);
        memset(&srcdata, 0, sizeof(SRC_DATA)); // MUST be zero initialized
    }

    src_reset(srcstate);
    srcdata.src_ratio = SAMPLERATE / SAMPLERATE_IN;
}

void jg_set_inputstate(jg_inputstate_t *ptr, int port) {
    input_device[port] = ptr;
}

void jg_set_gameinfo(jg_fileinfo_t info) {
    gameinfo = info;
}

void jg_set_auxinfo(jg_fileinfo_t info, int index) {
}

void jg_set_paths(jg_pathinfo_t paths) {
    pathinfo = paths;
}
